import './App.css';
import Booking from './Components/Booking/Booking';
import { ToastContainer } from 'react-toastify';

function App() {
  return (
    <div>
      <Booking />
      <ToastContainer />
    </div>
  );
}

export default App;
