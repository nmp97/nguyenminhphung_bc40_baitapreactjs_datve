import { BOOKING, DELETE_SEAT } from '../constants/seatConstant';

export const handleDeleteSeatAction = (id) => {
  return {
    type: DELETE_SEAT,
    payload: id,
  };
};

export const handleBookingAction = () => {
  return {
    type: BOOKING,
  };
};
