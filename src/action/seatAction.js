import { SEAT_SELECTED } from '../constants/seatConstant';

export const handleSeatSelectedAction = (seat) => {
  return {
    type: SEAT_SELECTED,
    payload: seat,
  };
};
