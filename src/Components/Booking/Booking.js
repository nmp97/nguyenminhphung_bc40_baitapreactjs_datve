import React, { Component } from 'react';
import Seats from '../Seats/Seats';
import Information from '../Information/Information';

export default class Booking extends Component {
  render() {
    return (
      <>
        <div className="overlay"></div>
        <div className="container py-1">
          <h2 className="text-center mb-4 pt-3">ĐẶT VÉ XEM PHIM</h2>
          <div className="row gx-5">
            <Seats />
            <Information />
          </div>
        </div>
      </>
    );
  }
}
